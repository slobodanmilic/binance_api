# Binance_api

Binanace_api was made with tasks in mind:

1. Print the top 5 symbols with quote asset BTC and the highest volume over the last 24 hours in descending order.
2. Print the top 5 symbols with quote asset USDT and the highest number of trades over the last 24 hours in descending order.
3. Using the symbols from Q1, what is the total notional value of the top 200 bids and asks currently on each order book?
4. What is the price spread for each of the symbols from Q2?
5. Every 10 seconds print the result of Q4 and the absolute delta from the previous value for each symbol.
6. Make the output of Q5 accessible by querying http://localhost:8080/metrics using the Prometheus Metrics format.

**Binance_api requires python3**

**How to setup environment**

**Create a new venv inside the binance_api directory**
- python3 -m venv venv
- source ./venv/bin/activate
- pip3 install -r requirements.txt

**How to use the program**
- python3 main.py
