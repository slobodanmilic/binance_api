import requests
import operator
import re
import time
from prometheus_client import start_http_server, Gauge

get_tiker= requests.get('https://api.binance.com/api/v3/ticker/24hr')
info = get_tiker.json()
symbols= info

def all_symbols():
    """Get all symbols from Binance"""

    info = get_tiker
    symbols = info.json()
    list_all_symbols=[]
    for i in symbols:
        list_all_symbols.append(i['symbol'])
    return list_all_symbols

def list_only_quote_asset(asset):
    """Lists all quote assets that have *asset in them except it self. For instance take BTC
    so it will make list of all quote assets that have *BTC but not list BTC coin
    Example how to use:
    list_only_quote_asset(str('BTC')
     """

    list_of_raw_quote=[]
    for i in all_symbols():
        list_of_raw_quote.append((re.findall(str(".*"+asset+"$"), str(i))))
    rem_empty_str = list(filter(None, list_of_raw_quote))
    join_to_one_list=[]

    for i in rem_empty_str:
        join_to_one_list.append(''.join(i))

    try:
        while True:
            join_to_one_list.remove(asset)
    except ValueError:
        pass
    return join_to_one_list


dic_res_qoute_vol = {}
def list_dict_quote_volume():
    """Loops through all quote assets pairs and checks if their
    value is the the same as quote asset from the symbol.
    If they have it then it will create a new dictionary
    that has keys symbol and second key pair which can be any pair key that
    ticker24 can provide"""

    list_sort = list_only_quote_asset(str('BTC'))  # gets all keypair *BTC
    for i in range(len(symbols)):

        for a in range(len(list_sort)):

            if list(symbols[i].values())[0] == str(list_sort[a]):

                crop_data = {key: (dict(symbols[i]))[key] for key in (dict(symbols[i])).keys()
                             & {'symbol', 'quoteVolume'}}
                dic_res_qoute_vol[crop_data.get('symbol')] = float(crop_data.get('quoteVolume'))

dic_res_count = {}
def list_dict_quote_count():
    """Loops through all quote assets pairs and checks if their
    value is the the same as quote asset from the symbol.
    If they have it then it will create a new dictionary
    that has keys symbol and count"""

    list_sort = list_only_quote_asset(str('USDT'))
    for i in range(len(symbols)):

        for a in range(len(list_sort)):

            if list(symbols[i].values())[0] == str(list_sort[a]):

                crop_data = {key: (dict(symbols[i]))[key] for key in (dict(symbols[i])).keys()
                             & {'symbol', 'count'}}
                dic_res_count[crop_data.get('symbol')] = float(crop_data.get('count'))
def run_first_task():
    """Print the top 5 symbols with quote asset BTC and the
    highest volume over the last 24 hours in descending order.
    """

    list_dict_quote_volume()
    print(dict(sorted(dic_res_qoute_vol.items(), key=operator.itemgetter(1), reverse=True)[:5]))

def run_second_task():
    """Print the top 5 symbols with quote asset USDT and the highest number of
    trades over the last 24 hours in descending order.
    """

    list_dict_quote_count()
    print(dict(sorted(dic_res_count.items(), key=operator.itemgetter(1), reverse=True)[:5]))

def run_third_task():
    """
    Using the symbols from Q1, what is the total notional value of the top 200
    bids and asks currently on each order book?
    """

    list_dict_quote_volume()
    data_from_task=dict(sorted(dic_res_qoute_vol.items(), key=operator.itemgetter(1), reverse=True)[:5])
    list_of_symbols = list(data_from_task.keys())


    def notinal_value(symbol):
        """Gets bids and asks for the current symbol. Gets 500 of them then uses the first 200.
        Because the API can get 100 than 500 of them.
        All data is rounded to have 8 digits"""

        order_book='https://api.binance.com/api/v3/depth?symbol={}&limit=500'.format(symbol)
        or_info = requests.get(str(order_book)).json()
        multiply_list_bids = []
        multiply_list_asks = []
        for i in or_info.get("bids")[:200]:
            multiply = round(float(i[0]) * float(i[1]), 8)
            multiply_list_bids.append(multiply)
        for i in or_info.get("asks")[:200]:
            multiply = round(float(i[0]) * float(i[1]), 8)
            multiply_list_asks.append(multiply)
        print('Top 200 total notional value of bids for ' + str(symbol) + ' is ' + str(round((sum(multiply_list_bids)), 8)))
        print('Top 200 total notional value of asks for ' + str(symbol) + ' is ' + str(round((sum(multiply_list_asks)), 8)))

    for i in list_of_symbols:
        notinal_value(str(i))
        time.sleep(0.1)#added wait because API sometimes hangs while I was testing

def price_spread():
    """Gets the price spread from the list dic_res_count sorts, then loops through every item"""

    pr_spread_list=[]
    data_from_task = dict(sorted(dic_res_count.items(), key=operator.itemgetter(1), reverse=True)[:5])
    list_of_symbols = list(data_from_task.keys())
    for i in list_of_symbols:
        order_book='https://api.binance.com/api/v3/ticker/bookTicker?symbol={}'.format(i)
        get_order_book = requests.get(order_book).json()
        or_book_bid = get_order_book.get("bidPrice")
        or_book_price = get_order_book.get("askPrice")
        pr_spread = abs(float(or_book_bid) - float(or_book_price))
        pr_spread_list.append({str(i): "{:.8f}".format(pr_spread)})

    return pr_spread_list

def run_forth_task():
    """
    Assignment  4. was:
    What is the price spread for each of the symbols from Q2?
    Function loops through every Symbol, gets the values from 2 keys
    (ask and bid price) then gets the abs difference
    between this to values
     """

    price_sp = price_spread()

    for i in price_sp:
        for key in i:
            print ("Price spread for {} is {}".format(key,i[key]))

pr_spread_list=[]

def run_fifth_task():
    """
    Every 10 seconds print the result of Q4 and the absolute delta from the previous value for each symbol
    The 10 seconds is implemented in the 6.task
    """

    pr_sp = price_spread()
    pr_spread_list.append(pr_sp)

    if len(pr_spread_list) == 1:
        """we need to have 2 items in the list to start referencing it
        so the first delta is 0 because we reference the same list"""
        pr_spread_list.append(pr_sp)
    else:
        pass

    pr_sp_dic = pr_spread_list[-2]
    res_dif_pr_spread_list = []

    for i, k in zip(pr_sp, pr_sp_dic):
        for s, e in zip(i, k):
            pri_sp_dif = abs(float(i[s]) - float(k[e]))
            res_dif_pr_spread_list.append({str(s): "{:.8f}".format(pri_sp_dif)})

    print("curent price spread                    {}".format(pr_sp))
    print("absolute delta from the previous value {}".format(res_dif_pr_spread_list))
    return res_dif_pr_spread_list

def name_of_gauge():
    """Helper function to get the keys from current price spread from pr_spread_list"""

    list_keys_price_spread=[]
    for i in pr_spread_list[-1]:
        for key in i:
            list_keys_price_spread.append(key)
    return list_keys_price_spread

def run_sixt_task():
    """
    Make the output of Q5 accessible by querying http://localhost:8080/metrics using the Prometheus Metrics format.
    """

    list_dict_quote_count()
    pr_sp_list_keys=name_of_gauge()
    g1 = Gauge('Curent_price_spread_for_{}'.format(pr_sp_list_keys[0]), 'Description of gauge')
    g2 = Gauge('Curent_price_spread_for_{}'.format(pr_sp_list_keys[1]), 'Description of gauge')
    g3 = Gauge('Curent_price_spread_for_{}'.format(pr_sp_list_keys[2]), 'Description of gauge')
    g4 = Gauge('Curent_price_spread_for_{}'.format(pr_sp_list_keys[3]), 'Description of gauge')
    g5 = Gauge('Curent_price_spread_for_{}'.format(pr_sp_list_keys[4]), 'Description of gauge')

    g6 = Gauge('Absolute_delta_for_{}'.format(pr_sp_list_keys[0]), 'Description of gauge')
    g7 = Gauge('Absolute_delta_for_{}'.format(pr_sp_list_keys[1]), 'Description of gauge')
    g8 = Gauge('Absolute_delta_for_{}'.format(pr_sp_list_keys[2]), 'Description of gauge')
    g9 = Gauge('Absolute_delta_for_{}'.format(pr_sp_list_keys[3]), 'Description of gauge')
    g10 = Gauge('Absolute_delta_or_{}'.format(pr_sp_list_keys[4]), 'Description of gauge')

    def make_cheks(list1, var2):
        """returns only a list that has var2 in it"""
        for i in range(len(list1)):
            for s in list1[i]:
                if s == str(var2):
                    return list1[i][s]

    # # g = Gauge('my_inprogress_requests', 'Description of gauge')#how to make a check example
    #
    # # @g.time()
    # def process_request(t):
    #     """A dummy function that takes some time."""
    #     time.sleep(t)

    def pre_process(t):
        """
        Gets data every t seconds and updates the monitoring data
        """

        absolute_delta_from_the_previous_value = run_fifth_task()
        curent_price_spread = pr_spread_list[-1]
        pr_sp_list_keys = name_of_gauge()

        g1.set(make_cheks(curent_price_spread, str(pr_sp_list_keys[0])))
        g2.set(make_cheks(curent_price_spread, str(pr_sp_list_keys[1])))
        g3.set(make_cheks(curent_price_spread, str(pr_sp_list_keys[2])))
        g4.set(make_cheks(curent_price_spread, str(pr_sp_list_keys[3])))
        g5.set(make_cheks(curent_price_spread, str(pr_sp_list_keys[4])))

        g6.set(make_cheks(absolute_delta_from_the_previous_value, str(pr_sp_list_keys[0])))
        g7.set(make_cheks(absolute_delta_from_the_previous_value, str(pr_sp_list_keys[1])))
        g8.set(make_cheks(absolute_delta_from_the_previous_value, str(pr_sp_list_keys[2])))
        g9.set(make_cheks(absolute_delta_from_the_previous_value, str(pr_sp_list_keys[3])))
        g10.set(make_cheks(absolute_delta_from_the_previous_value, str(pr_sp_list_keys[4])))
        time.sleep(t)

    while True:

        pre_process(10)

def main():
    print ("First task")
    run_first_task()

    print("\nSecond task")
    run_second_task()

    print("\nThird task")
    run_third_task()

    print("\nForth task")
    run_forth_task()

    print("\nFifth and six task")
    run_fifth_task()
    run_sixt_task()
if __name__ == '__main__':
    try:
        start_http_server(8080)
        main()
    except KeyboardInterrupt as err:
        print("Shuting Down App. Good Bye!")
        exit(0)
    except Exception as err:
        print("Error ocurred: ", err)
        exit(1)